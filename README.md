# PROJECT INFO
TASK MANAGER

# DEVELOPER INFO
NAME: ALEXEY TYUNYAEV

E-MAIL: alex.t-pnz@mail.ru

# SOFTWARE
- JDK 11
- MS WINDOWS 10

# PROGRAM RUN
```bash
java -jar ./task-manager.jar
```
