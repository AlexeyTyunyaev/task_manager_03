package ru.tyunyaev.tm;
import ru.tyunyaev.tm.execution.Parser;

import static ru.tyunyaev.tm.execution.InformationOutput.welcomeText;

public class Application {

    public static void main(String[] args) {
        Parser parser = new Parser(args);

        welcomeText();
        parser.startParseArgs();
    }
}
