package ru.tyunyaev.tm.interfaces;

public interface ITerminalConst {
    String VERSION = "1.0.0.0";
    String FIRST_NAME = "Alexey";
    String LAST_NAME = "Tyunyaev";
    String E_MAIL = "alex.t-pnz@mail.ru";

    String VERSION_DESCRIPTION = "Show version info.";
    String ABOUT_DESCRIPTION = "Show developer info";
    String HELP_DESCRIPTION = "Display terminal commands";

    String WELCOME_TEXT = "*** WELCOME TO TASK MANAGER ***";

    String CMD_HELP = "help";
    String CMD_ABOUT = "about";
    String CMD_VERSION = "version";
}
