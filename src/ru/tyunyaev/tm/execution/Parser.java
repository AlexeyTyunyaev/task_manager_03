package ru.tyunyaev.tm.execution;

import ru.tyunyaev.tm.interfaces.ITerminalConst;

import static ru.tyunyaev.tm.execution.InformationOutput.*;

public class Parser implements ITerminalConst {
    private final String[] args;
    private String arg;

    public Parser(String[] args) {
        this.args = args;
    }

    public void startParseArgs() {
        if (checkArgs()) { parseArgs(); }
        else { noArguments();}
    }

    private boolean checkArgs() {
        if (args == null || args.length == 0) {
            return false;
        } else {
            arg = args[0];
            return arg != null && !arg.isEmpty();
        }
    }

    private void parseArgs() {
        switch (arg) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            default:
                unknownArgument();
                break;
        }
    }
}
