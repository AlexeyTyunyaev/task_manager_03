package ru.tyunyaev.tm.execution;

import ru.tyunyaev.tm.interfaces.ITerminalConst;

public class InformationOutput implements ITerminalConst {
    public static void displayHelp(){
        System.out.println("[HELP]");
        System.out.println("version - " + VERSION_DESCRIPTION);
        System.out.println("about - " + ABOUT_DESCRIPTION);
        System.out.println("help - " + HELP_DESCRIPTION);
    }

    public static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("VERSION: " + VERSION);
    }

    public static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: " + FIRST_NAME + " " + LAST_NAME);
        System.out.println("E-MAIL: " + E_MAIL);
    }

    public static void unknownArgument() {
        System.out.println("Unknown argument.");
        System.out.println("To get a list of all arguments, enter the 'help' argument.");
    }

    public static void welcomeText() {
        System.out.println(WELCOME_TEXT);
    }

    public static void noArguments() {
        System.out.println("No arguments passed");
    }
}
